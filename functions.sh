#!/bin/sh -e

function before_script
{
  # Install packages
  apt-get update -qq && apt-get install -y -qq zip unzip wget git

  # Install composer dependencies
  wget https://composer.github.io/installer.sig -O - -q | tr -d '\n' > installer.sig
  php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
  php -r "if (hash_file('SHA384', 'composer-setup.php') === file_get_contents('installer.sig')) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
  php composer-setup.php
  php -r "unlink('composer-setup.php'); unlink('installer.sig');"
}

function build
{
  SOURCE_DIR="src"
  BUILD_DIR="build"
  ARTIFACTS_DIR="artifacts"

  # create directories
  [ -d $BUILD_DIR ] || mkdir $BUILD_DIR
  [ -d $ARTIFACTS_DIR ] || mkdir $ARTIFACTS_DIR

  # fetch current version
  VERSION=`cat "$SOURCE_DIR/meta.xml" | grep -Po '(?<=<version>)[0-9\-.]+'`

  # fetch extension ID
  ID=`cat "$SOURCE_DIR/meta.xml" | grep -Po '(?<=<id>)[^<]+'`

  # fetch latest build release
  RELEASE=$CI_PIPELINE_ID

  # build dependencies
  if [ -f composer.json ]
  then
    export COMPOSER_NO_INTERACTION=1
    php composer.phar --ignore-platform-reqs --prefer-dist --no-dev install
  fi

  # prepare content of build directory
  cp -r $SOURCE_DIR/* $BUILD_DIR

  for FILE in CHANGES.md DESCRIPTION.md
  do
      [ -f "$FILE" ] && [ ! -f "$BUILD_DIR/$FILE" ] && cp "$FILE" "$BUILD_DIR"
  done

  sed -ir "s/{{RELEASE}}/$RELEASE/" "$BUILD_DIR/meta.xml"
  rm -f "$BUILD_DIR/meta.xmlr"

  [ -d "$BUILD_DIR/$MODULE/_meta" ] || mkdir "$BUILD_DIR/$MODULE/_meta"
  date "+%Y-%m-%d %H:%M:%S" > "$BUILD_DIR/_meta/.build"
  git rev-parse HEAD > "$BUILD_DIR/_meta/.revision"

  # pack extension
  cd $BUILD_DIR
  zip -r "../$ARTIFACTS_DIR/$ID-$VERSION-$RELEASE.zip" *
  cp "../$ARTIFACTS_DIR/$ID-$VERSION-$RELEASE.zip" "../$ARTIFACTS_DIR/$ID-latest.zip"

  # cleanup
  cd ../
  rm -Rf $BUILD_DIR
}
